#include <inttypes.h>
#include <stdio.h>
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static void test_1(void* heap){
  //Default block allocation

  fprintf(stdout, "Test 1. Memory allocation\n");

  int64_t* array_int = _malloc(sizeof(int64_t) * 8);

  fprintf(stderr, "Test 1: allocation\n");
  debug_heap(stderr, heap);

  if (!array_int){
	err("Test 1: Failed at block allocate: null block\n");
  }
  _free(array_int);
  fprintf(stderr, "Test 1: freed\n");
  debug_heap(stderr, heap);
  fprintf(stdout, "Test 1 passed\n.");
}

static void test_2(void* heap){
  //Free a block from multiple allocated

  fprintf(stdout, "Test 2. Memory allocation: free a block\n");

  int64_t* f_block = _malloc(sizeof(int64_t) * 8);
  int64_t* s_block = _malloc(sizeof(int64_t) * 8);

  fprintf(stderr, "Test 2: allocation\n");
  debug_heap(stderr, heap);

  if (!f_block || !s_block){
	err("Test 2: Failed at blocks allocate: null block\n");
  }
  _free(f_block);
  struct block_header * f_block_head = block_get_header(f_block);
  fprintf(stderr, "Test 2: free a block\n");
  debug_heap(stderr, heap);  

  struct block_header * s_block_head = block_get_header(s_block);

  if (!f_block_head->is_free || s_block_head->is_free){
	err("Test 2: ucorrect release\n");
  }
  
  _free(s_block);
  debug_heap(stderr, heap);
  fprintf(stdout, "Test 2 passed.\n");
}

static void test_3(void* heap){
  //Free 2 blocks from multiple allocated

  fprintf(stdout, "Test 3. Memory allocation: free 2 blocks\n");

  int64_t* f_block = _malloc(sizeof(int64_t) * 8);
  int64_t* s_block = _malloc(sizeof(int64_t) * 8);
  int64_t* t_block = _malloc(sizeof(int64_t) * 8);

  fprintf(stderr, "Test 3: allocation\n");
  debug_heap(stderr, heap);

    if (!f_block || !s_block || !t_block)
	err("Test 3: Failed at blocks allocate: null block");
  
  _free(f_block);
  fprintf(stderr, "Test 3: free a block\n");
  debug_heap(stderr, heap);

  _free(s_block);
  fprintf(stderr, "Test 3: free a block\n");
  debug_heap(stderr, heap);

  struct block_header * f_block_head = block_get_header(f_block);
  struct block_header * s_block_head = block_get_header(s_block);
  struct block_header * t_block_head = block_get_header(t_block);

  if (!f_block_head->is_free || !s_block_head->is_free || t_block_head->is_free)
	err("Test 3: ucorrect release\n");
 
  _free(t_block);
  debug_heap(stderr, heap);
  fprintf(stdout, "Test 3 passed\n");
}

static void test_4(void* heap){
  //Allocate a new region (expanded old region)

  fprintf(stdout, "Test 4. Memory allocation: allocate new region\n");
  
  int64_t* f_block = _malloc(sizeof(int64_t) * 50);

  fprintf(stderr, "Test 4: allocation 1 block:\n");
  debug_heap(stderr, heap);

  int64_t* s_block = _malloc(sizeof(int64_t) * 1050);
 
  fprintf(stderr, "Test 4: allocation 2 block:\n");
  debug_heap(stderr, heap);

  _free(f_block);
  _free(s_block);

  debug_heap(stderr, heap);
  fprintf(stdout, "Test 4 passed\n");
}

static void test_5(void* heap){
  //Allocate a new region (the old region is not expanding)

  fprintf(stdout, "Test 5. Memory allocation: allocate new region\n");
  
  void* block = _malloc(50);

  fprintf(stderr, "Test 4: allocation 1 block:\n");
  debug_heap(stderr, heap);

  struct block_header * block_head = block_get_header(block);

  void* test_block_head = block_head + 10000;

  mmap(test_block_head, 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0);

  _malloc(10050);
  debug_heap(stderr, heap);

  _free(block);
  debug_heap(stderr, heap);

  fprintf(stdout, "Test 5 passed\n");
}

void test_all(void* heap){
  test_1(heap);
  test_2(heap);
  test_3(heap);
  test_4(heap);
  test_5(heap);
}
