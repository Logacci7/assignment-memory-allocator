#include "tests.h"

int main(void){

  printf("Heap initialisation...\n");
  void* heap = heap_init(REGION_MIN_SIZE);
  debug_heap(stderr, heap);
  if (!heap){
	err("Heap init failed");
	return 1;
  }
  fprintf(stdout, "Initialisation success.");

  test_all(heap);
  
  return 0;
}




